Setting up Mailman Dev Environment
==================================

This project eases the development setup of GNU Mailman project. It uses the
`repo` tool from android project to download all the source code and then there
is a bash script that you can use to setup your virtualenv and everything.

Requirements:
- Python3.5/Python3.4
- Python2.7
- Pip
- virtualenv (install using pip install virtualenv)


Install Repo
------------

[Repo][2] is a small wrapper over git to manage source codes with multiple git
repositories. You can install it this way:

```bash
$ mkdir ~/bin
$ export PATH="$PATH:$HOME/bin"
$ curl https://storage.googleapis.com/git-repo-downloads/repo > ~/bin/repo
$ chmod a+x ~/bin/repo
```

You can also add the export line to your `.bashrc` or `.zshrc` so that you don't
have to do that everytime.

Download the source code
------------------------

You can download all the source code with one single command:

```bash
$ mkdir mailman3; cd mailman3
$ repo init -u https://gitlab.com/maxking/mailman-manifest
$ repo sync
```

This will take a while.

Setup vietualenvs and source code:
----------------------------------

There is a tool `virtualenvwrapper` that makes using virtualenvironments
extremely simple. You can install that:

```bash
$ sudo pip install virtualenvwrapper
$ source /usr/local/bin/virtualenvwrapper.sh
```

Then to setup everything, run:
```bash
$ scripts/bootstrap
```

It will take some time to download and install all the dependencies, but after
it is done, you will have a complete dev environment.

What Actually Happened:
----------------------

What happened was no magic, we just used the `repo` tool to sync all the git
repos in one single go and then I wrote a small bash script to do the same thing
we do everytime to setup python environments.

`Virtualenvwrapper`[1] is a fantastic tools that I highly recommend that you use
if you are dealing with python virtualenvs. It creates virtualenvs in a single
place (`~/.virutalenvs` by default, but you can change that.) and you can switch
between virtualenvs with a single command without any need to do
`vevn/bin/activate` from the repo. You can just do `workon py3` from anywhere
and you have your environment up.

Bootstrap scipt creates two virtualenvironments for you, `py3` and `py2` which
are usually located in `~/.virtualenvs/py2` and `~/.virtualenvs/py3`. You can
switch between then with `workon <envname>` command.


[1]: https://virtualenvwrapper.readthedocs.io/en/latest/
[2]: https://source.android.com/source/using-repo.html
